#include <linux/version.h>
#include <linux/autoconf.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/sched.h>
#include <linux/kernel.h>  /* printk() */
#include <linux/errno.h>   /* error codes */
#include <linux/types.h>   /* size_t */
#include <linux/vmalloc.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/wait.h>
#include <linux/file.h>
// #include <linux/fdtable.h>	/* struct request_queue */
				// added to run on real machine.

#include "spinlock.h"
#include "osprd.h"

/* The size of an OSPRD sector. */
#define SECTOR_SIZE	512

/* This flag is added to an OSPRD file's f_flags to indicate that the file
 * is locked. */
#define F_OSPRD_LOCKED	0x80000

/* Define eprintk() to be a version of printk(), which prints messages to
 * the console.
 * (If working on a real Linux machine, change KERN_NOTICE to KERN_ALERT or
 * KERN_EMERG so that you are sure to see the messages.  By default, the
 * kernel does not print all messages to the console.  Levels like KERN_ALERT
 * and KERN_EMERG will make sure that you will see messages.) */
// #define eprintk(format, ...) printk(KERN_NOTICE format, ## __VA_ARGS__)
#define eprintk(format, ...) printk(KERN_ALERT format, ## __VA_ARGS__)

MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("CS 5460 RAM Disk");
// EXERCISE: Pass your names into the kernel as the module's authors.
MODULE_AUTHOR("Chung Hwan Kim");

#define OSPRD_MAJOR	222

/* This module parameter controls how big the disk will be.
 * You can specify module parameters when you load the module,
 * as an argument to insmod: "insmod osprd.ko nsectors=4096" */
static int nsectors = 32;
module_param(nsectors, int, 0);

/* ramdisk lock info (for deadlock detection) */
typedef struct osprd_lock {
	struct task_struct* task;	// lock holder
	int type;					// type of lock: read(0), write(1)
	struct list_head link;		// the head of the lock queue
} osprd_lock_t;

/* The internal representation of our device. */
typedef struct osprd_info {
	uint8_t *data;                  // The data array. Its size is
	                                // (nsectors * SECTOR_SIZE) bytes.

	osp_spinlock_t mutex;           // Mutex for synchronizing access to
					// this block device
	
	wait_queue_head_t blockq;       // Wait queue for tasks blocked on
					// the device lock
	
	/* HINT: You may want to add additional fields to help 
	         in detecting deadlock or enforcing fairness!  
	*/
	
	/* fairness/bounded wait fixed! */
	uint64_t rlocks;	// number of read locks held
	uint64_t wlocks;	// number of write locks held

	/* for deadlock detection */
	osprd_lock_t lockq;		// lock info queue
	osp_spinlock_t lockq_mutex;	// mutex for lock info queue
	
	// The following elements are used internally; you don't need
	// to understand them.
	struct request_queue *queue;    // The device request queue.
	spinlock_t qlock;		// Used internally for mutual
	                                //   exclusion in the 'queue'.
	struct gendisk *gd;             // The generic disk.
} osprd_info_t;

#define NOSPRD 4
static osprd_info_t osprds[NOSPRD];


// Declare useful helper functions
static osprd_info_t *file2osprd(struct file *filp);
static void for_each_open_file(struct task_struct *task,
			       void (*hook)(struct file *filp, osprd_info_t *d),
			       osprd_info_t *d);

// lock info queue management functions
static int osprd_add_lock(osprd_info_t*, struct task_struct*, int);
static void osprd_remove_lock(osprd_info_t*, struct task_struct*);
static osprd_lock_t* osprd_get_lock(osprd_info_t*, struct task_struct*);
static osprd_lock_t* osprd_get_lock_at(osprd_info_t*, int);

/*
 * osprd_process_request(d, req)
 *   Called when the user reads or writes a sector.
 *   Should perform the read or write, as appropriate.
 */
static void osprd_process_request(osprd_info_t *d, struct request *req)
{
	// variables added here
	uint64_t offset, size;
	uint8_t* buf;

	if (!blk_fs_request(req)) {
		end_request(req, 0);
		return;
	}

	// EXERCISE: Perform the read or write request by copying data between
	// our data array and the request's buffer.
	// Hint: The 'struct request' argument tells you what kind of request
	// this is, and which sectors are being read or written.
	// Read about 'struct request' in <linux/blkdev.h>.
	// Consider the 'req->sector', 'req->current_nr_sectors', and
	// 'req->buffer' members, and the rq_data_dir() function.

	// Your code here.
//	eprintk("Should process request...\n");
	
	// calculate the offset and the size to read or write
	offset = req->sector * SECTOR_SIZE;
	buf = d->data + offset;
	size = req->current_nr_sectors * SECTOR_SIZE;

	// is this a write request?
	if (rq_data_dir(req))
	{
		// yes, let's write!
		memcpy(buf, req->buffer, size);

		eprintk("[%u] write process - offset: %llu, size: %llu\n",
			current->pid, offset, size);
	}
	else
	{
		// no, let's read!
		memcpy(req->buffer, buf, size);

		eprintk("[%u] read process - offset: %llu, size: %llu\n",
			current->pid, offset, size);
	}

	end_request(req, 1);
}

// do stuffs to block the current task. this function blocks itself, and
// returns when wake_up() is called on lock-release.
static int osprd_block_task(osprd_info_t *d, wait_queue_t *wait,
	struct file *filp, int filp_writable)
{
	// add this task to wait queue as exclusive.
	prepare_to_wait_exclusive(&d->blockq, wait, TASK_INTERRUPTIBLE);

	eprintk("[%u] %s task blocked! (added to wait queue)\n", current->pid, 
		(filp_writable ? "write" : "read"));

	// give control back to scheduler.
	schedule();	// this will block this task immediately!

	// ... ok, control is back! lock-acquire function in user-mode is still 
	// blocked because this function has not yet returned.
	
	// let's remove this task from wait queue.
	finish_wait(&d->blockq, wait);

	eprintk("[%u] %s task awake! (removed from wait queue)\n", 
		current->pid, (filp_writable ? "write" : "read"));
			
	// does this task have pending signals?
	if (signal_pending(current))
	{
		// -- lock the mutex --
		osp_spin_lock(&d->mutex);

		// decrement back the corresponding lock count.
		(filp_writable) ? d->wlocks-- : d->rlocks--;
		
		// mark back unlocked on the file flag.
		filp->f_flags &= (~F_OSPRD_LOCKED);
		
		// -- unlock the mutex --
		osp_spin_unlock(&d->mutex);
		
		eprintk("[%u] %s lock acquire failed - "\
			"we thought succeeded but pending signals.. "\
			"restarting lock acquire all over..\n",
			current->pid, (filp_writable ? "write" : "read"));

		// this function will be re-executed all over.
		return -ERESTARTSYS;
	}

	return 0;
}

// do stuffs to acquire a lock.
static int osprd_acquire_lock(osprd_info_t *d, wait_queue_t *wait, 
	struct file *filp, int filp_writable)
{
	int r, block = 0;
	
	eprintk("[%u] %s lock acquire requested - lock counts(r/w): %llu/%llu\n",
			current->pid, (filp_writable ? "write" : "read"), 
			d->rlocks, d->wlocks);

	// is the file already holding a lock?
	if (filp->f_flags & F_OSPRD_LOCKED)
	{
		eprintk("[%u] %s lock acquire failed - already holding a lock\n",
				current->pid, (filp_writable ? "write" : "read"));

		return -EDEADLK;
	}

	// is the file writable?
	if (filp_writable)
	{
		// yes, then it is a write lock request!
	
		// -- lock the mutex --
		osp_spin_lock(&d->mutex);

		// is any other file holding a write or a read lock?
		if (d->rlocks > 0 || d->wlocks > 0)
		{
			// yes, block this file!
			block = 1;
		}

		// increment write lock count.
		d->wlocks++;

		// -- unlock the mutex --
		osp_spin_unlock(&d->mutex);
	}
	else
	{
		// no, then it is a read lock request!
	
		// -- lock the mutex --
		osp_spin_lock(&d->mutex);

		// is any other file holding a write lock?
		if (d->wlocks > 0)
		{
			// yes, block this file!
			block = 1;
		}

		// increment read lock count.
		d->rlocks++;

		// -- unlock the mutex --
		osp_spin_unlock(&d->mutex);
	}

	// mark locked on the file flag.
	filp->f_flags |= F_OSPRD_LOCKED;

	eprintk("[%u] %s lock acquire succeeded - lock counts(r/w): %llu/%llu\n",
		current->pid, (filp_writable ? "write" : "read"),
		d->rlocks, d->wlocks);
	
	/* for deadlock detection */
	// add this lock info to lock info queue.
	if ((r = osprd_add_lock(d, current, filp_writable)) != 0)
	{
		return r;
	}

	// should we block current task?
	if (block)
	{
		// yes, do stuffs to block this task.
		if ((r = osprd_block_task(d, wait, filp, filp_writable)) != 0)
		{
			return r;
		}
	}

	// lock request granted successfully, so return 0.
	return 0;
}

// do stuffs to try acquiring a lock.
static int osprd_try_acquire_lock(osprd_info_t *d, wait_queue_t *wait,
	struct file *filp, int filp_writable)
{
	eprintk("[%u] %s lock try-acquire requested - "\
			"lock counts(r/w): %llu/%llu\n",
			current->pid, (filp_writable ? "write" : "read"), 
			d->rlocks, d->wlocks);

	// is the file already holding a lock?
	if (filp->f_flags & F_OSPRD_LOCKED)
	{
		eprintk("[%u] %s lock try-acquire failed - already holding a lock\n",
			current->pid, (filp_writable ? "write" : "read"));

		return -EBUSY;
	}
	
	// is the file writable?
	if (filp_writable)
	{
		// yes, then try acquiring a write lock!
		
		// -- lock the mutex --
		osp_spin_lock(&d->mutex);
		
		// is any other file holding a write or a read lock?
		if (d->rlocks > 0 || d->wlocks > 0)
		{
			// -- unlock the mutex --
			osp_spin_unlock(&d->mutex);
		
			eprintk("[%u] write lock try-acquire failed - "\
				"read or write lock held by other task\n", current->pid);

			return -EBUSY;	
		}
		
		// increment write lock count.
		d->wlocks++;
		
		// -- unlock the mutex --
		osp_spin_unlock(&d->mutex);
	}
	else
	{
		// no, then try acquiring a read lock!
		
		// -- lock the mutex --
		osp_spin_lock(&d->mutex);

		// is any other file holding a write lock?
		if (d->wlocks > 0)
		{
			// -- unlock the mutex --
			osp_spin_unlock(&d->mutex);
			
			eprintk("[%u] read lock try-acquire failed - "\
				"write lock held by other task\n", current->pid);
				
			return -EBUSY;
		}

		// increment read lock count.
		d->rlocks++;

		// -- unlock the mutex --
		osp_spin_unlock(&d->mutex);
	}

	// mark locked on the file flag.
	filp->f_flags |= F_OSPRD_LOCKED;

	eprintk("[%u] %s lock try-acquire succeeded - "\
		"lock counts(r/w): %llu/%llu\n",
		current->pid, (filp_writable ? "write" : "read"),
		d->rlocks, d->wlocks);
	
	// lock request granted successfully, so return 0.
	return 0;
}

// do stuffs to release a lock.
static int osprd_release_lock(osprd_info_t *d, wait_queue_t *wait,
	struct file *filp, int filp_writable)
{
	eprintk("[%u] %s lock release requested - lock counts(r/w): %llu/%llu\n",
		current->pid, (filp_writable ? "write" : "read"),
		d->rlocks, d->wlocks);
	
	// is this file holding a lock?
	if (!(filp->f_flags & F_OSPRD_LOCKED))
	{
		eprintk("[%u] %s lock release failed - not a lock holder\n",
			current->pid, (filp_writable ? "write" : "read"));

		return -EINVAL;
	}

	// -- lock the mutex --
	osp_spin_lock(&d->mutex);

	// decrement the corresponding lock count.
	(filp_writable) ? d->wlocks-- : d->rlocks--;
	
	// -- unlock the mutex --
	osp_spin_unlock(&d->mutex);
	
	// mark unlocked on the file flag.
	filp->f_flags &= (~F_OSPRD_LOCKED);

	eprintk("[%u] %s lock release succeeded - lock counts(r/w): %llu/%llu\n",
		current->pid, (filp_writable ? "write" : "read"),
		d->rlocks, d->wlocks);
			
	/* for deadlock detection */
	// remove this lock info from lock info queue.
	osprd_remove_lock(d, current);

	// is any other task waiting?
	if (waitqueue_active(&d->blockq))
	{
		eprintk("[%u] waking up next task on wait queue..\n", current->pid);

		// wake up the next lock holder.
		wake_up(&d->blockq);		
	}

	// lock released successfully, return 0.
	return 0;
}

// This function is called when a /dev/osprdX file is opened.
// You aren't likely to need to change this.
static int osprd_open(struct inode *inode, struct file *filp)
{
	// Always set the O_SYNC flag. That way, we will get writes immediately
	// instead of waiting for them to get through write-back caches.
	filp->f_flags |= O_SYNC;
	return 0;
}

// This function is called when a /dev/osprdX file is finally closed.
// (If the file descriptor was dup2ed, this function is called only when the
// last copy is closed.)
static int osprd_close_last(struct inode *inode, struct file *filp)
{
	osprd_info_t *d;
	int filp_writable;
	
	DEFINE_WAIT(wait);		// wait queue entry in case we block
	wait.func = &default_wake_function;

	if (filp) {
		d = file2osprd(filp);
		filp_writable = filp->f_mode & FMODE_WRITE;
		
		// EXERCISE: If the user closes a ramdisk file that holds
		// a lock, release the lock.  Also wake up blocked processes
		// as appropriate.

		// Your code here.

		// is this file holding a lock?
		if (filp->f_flags & F_OSPRD_LOCKED)
		{
			// yes, then do stuffs to release the lock.
			osprd_release_lock(d, &wait, filp, filp_writable);
		}
		
		// This line avoids compiler warnings; you may remove it.
	//	(void) filp_writable, (void) d;
	}
	
	return 0;
}

/*
 * osprd_ioctl(inode, filp, cmd, arg)
 *   Called to perform an ioctl on the named file.
 */
int osprd_ioctl(struct inode *inode, struct file *filp,
		unsigned int cmd, unsigned long arg)
{
	int r, filp_writable;

	osprd_info_t *d = file2osprd(filp);	// device info
	DEFINE_WAIT(wait);		// wait queue entry in case we block
	wait.func = &default_wake_function;
	r = 0;			// return value: initially 0

	// is file open for writing?
	filp_writable = (filp->f_mode & FMODE_WRITE) != 0;

	// This line avoids compiler warnings; you may remove it.
//	(void) filp_writable, (void) d;
	
	// Set 'r' to the ioctl's return value: 0 on success, negative on error

	if (cmd == OSPRDIOCACQUIRE) {
		
		// EXERCISE: Lock the ramdisk.
		//
		// If *filp is a writable file, then attempt to write-lock
		// the ramdisk; otherwise attempt to read-lock the ramdisk.
		//
        // This lock request must block using 'd->blockq' until:
		// 1) no other process holds a write lock;
		// 2) either the request is for a read lock, or no other process
		//    holds a read lock; and
		// 3) lock requests should be serviced in order, so no process
		//    that blocked earlier is still blocked waiting for the
		//    lock.
		//
		// If a process acquires a lock, mark this fact by setting
		// 'filp->f_flags |= F_OSPRD_LOCKED'.  You may also need to
		// keep track of how many read and write locks are held:
		// change the 'osprd_info_t' structure to do this.
		//
		// Also wake up processes waiting on 'd->blockq' as needed.
		//
		// If the lock request would cause a deadlock, return -EDEADLK.
		// If the lock request blocks and is awoken by a signal, then
		// return -ERESTARTSYS.
		// Otherwise, if we can grant the lock request, return 0.

		// Your code here (instead of the next two lines).
	//	eprintk("Attempting to acquire\n");		
	//	r = -ENOTTY;

		// do stuffs to acquire the lock.
		r = osprd_acquire_lock(d, &wait, filp, filp_writable);
		
	} else if (cmd == OSPRDIOCTRYACQUIRE) {
		
		// EXERCISE: ATTEMPT Lock the ramdisk.
		//
		// This is just like OSPRDIOCACQUIRE, except it should never
		// block.  If OSPRDIOCACQUIRE would block or return deadlock,
		// OSPRDIOCTRYACQUIRE should return -EBUSY.
		// Otherwise, if we can grant the lock request, return 0.

		// Your code here (instead of the next two lines).
	//	eprintk("Attempting to try acquire\n");
	//	r = -ENOTTY;
		
		// do stuffs to try acquiring the lock.
		r = osprd_try_acquire_lock(d, &wait, filp, filp_writable);

	} else if (cmd == OSPRDIOCRELEASE) {
		
		// EXERCISE: Unlock the ramdisk.
		//
		// If the file hasn't locked the ramdisk, return -EINVAL.
		// Otherwise, clear the lock from filp->f_flags, wake up
		// the wait queue, perform any additional accounting steps
		// you need, and return 0.
		
		// Your code here (instead of the next line).
	//	r = -ENOTTY;

		// do stuffs to release the lock.
		r = osprd_release_lock(d, &wait, filp, filp_writable);

	} else
		r = -ENOTTY; /* unknown command */
	return r;
}


// Initialize internal fields for an osprd_info_t.

static void osprd_setup(osprd_info_t *d)
{
	/* Initialize the wait queue. */
	init_waitqueue_head(&d->blockq);
	osp_spin_lock_init(&d->mutex);

	/* Add initialization code here if you add fields to osprd_info_t. */

	// initialize read and write lock counts.
	d->rlocks = 0;
	d->wlocks = 0;
	INIT_LIST_HEAD(&d->lockq.link);
	d->lockq.task = NULL;
	d->lockq.type = -1;
	osp_spin_lock_init(&d->lockq_mutex);
}

// allocates a new lock info and add it to the tail of lock queue.
static int osprd_add_lock(osprd_info_t *d, struct task_struct* task, int type)
{
	osprd_lock_t* lock = (osprd_lock_t*)vmalloc(sizeof(osprd_lock_t));
	if (lock == NULL)
	{
		return -ENOBUFS;
	}
	
	INIT_LIST_HEAD(&lock->link);
	lock->task = task;
	lock->type = type;
	
	osp_spin_lock(&d->lockq_mutex);

	list_add_tail(&lock->link, &d->lockq.link);
	
	osp_spin_unlock(&d->lockq_mutex);

	return 0;
}

// deletes the lock info that the specified task is holding from lock queue.
static void osprd_remove_lock(osprd_info_t *d, struct task_struct* task)
{
	osprd_lock_t* lock;
	struct list_head *pos;
	
	osp_spin_lock(&d->lockq_mutex);

	list_for_each(pos, &d->lockq.link)
	{
		lock = list_entry(pos, osprd_lock_t, link);
		if (lock->task == task || lock->task->pid == task->pid)
		{
			list_del(pos);
			vfree(lock);
		
			osp_spin_unlock(&d->lockq_mutex);
			return;
		}
	}
	
	osp_spin_unlock(&d->lockq_mutex);
}

// returns the lock info that the specified task is holding on lock queue.
static osprd_lock_t* osprd_get_lock(osprd_info_t *d, struct task_struct* task)
{
	osprd_lock_t* lock;
	struct list_head *pos;
	
	osp_spin_lock(&d->lockq_mutex);

	list_for_each(pos, &d->lockq.link)
	{
		lock = list_entry(pos, osprd_lock_t, link);
		if (lock->task->pid == task->pid)
		{
			osp_spin_unlock(&d->lockq_mutex);
			return lock;
		}
	}
	
	osp_spin_unlock(&d->lockq_mutex);

	return NULL;
}

// returns the lock info at the specfied index on block queue.
static osprd_lock_t* osprd_get_lock_at(osprd_info_t *d, int index)
{
	osprd_lock_t* lock;
	struct list_head *pos;
	int i = 0;
	
	osp_spin_lock(&d->lockq_mutex);

	list_for_each(pos, &d->lockq.link)
	{
		lock = list_entry(pos, osprd_lock_t, link);
		if (i == index)
		{
			osp_spin_unlock(&d->lockq_mutex);
			return lock;
		}

		i++;
	}
	
	osp_spin_unlock(&d->lockq_mutex);

	return NULL;
}

/*****************************************************************************/
/*          THERE IS NO NEED TO CHANGE ANY CODE BELOW THIS LINE!             */
/*                                                                           */
/*****************************************************************************/

// Process a list of requests for a osprd_info_t.
// Calls osprd_process_request for each element of the queue.

static void osprd_process_request_queue(struct request_queue *q)
/* 'request_queue_t -> struct request_queue' to run on real machine. */
{
	osprd_info_t *d = (osprd_info_t *) q->queuedata;
	struct request *req;

	while ((req = elv_next_request(q)) != NULL)
		osprd_process_request(d, req);
}


// Some particularly horrible stuff to get around some Linux issues:
// the Linux block device interface doesn't let a block device find out
// which file has been closed.  We need this information.

static struct file_operations osprd_blk_fops;
static int (*blkdev_release)(struct inode *, struct file *);

static int _osprd_release(struct inode *inode, struct file *filp)
{
	if (file2osprd(filp))
		osprd_close_last(inode, filp);
	return (*blkdev_release)(inode, filp);
}

static int _osprd_open(struct inode *inode, struct file *filp)
{
	if (!osprd_blk_fops.open) {
		memcpy(&osprd_blk_fops, filp->f_op, sizeof(osprd_blk_fops));
		blkdev_release = osprd_blk_fops.release;
		osprd_blk_fops.release = _osprd_release;
	}
	filp->f_op = &osprd_blk_fops;
	return osprd_open(inode, filp);
}
		

// The device operations structure.

static struct block_device_operations osprd_ops = {
	.owner = THIS_MODULE,
	.open = _osprd_open,
	// .release = osprd_release, // we must call our own release
	.ioctl = osprd_ioctl
};


// Given an open file, check whether that file corresponds to an OSP ramdisk.
// If so, return a pointer to the ramdisk's osprd_info_t.
// If not, return NULL.

static osprd_info_t *file2osprd(struct file *filp)
{
	if (filp) {
		struct inode *ino = filp->f_dentry->d_inode;
		if (ino->i_bdev
		    && ino->i_bdev->bd_disk
		    && ino->i_bdev->bd_disk->major == OSPRD_MAJOR
		    && ino->i_bdev->bd_disk->fops == &osprd_ops)
			return (osprd_info_t *) ino->i_bdev->bd_disk->private_data;
	}
	return NULL;
}


// Call the function 'hook' with data 'd' for each of 'task's open files.

static void for_each_open_file(struct task_struct *task,
			  void (*hook)(struct file *filp, osprd_info_t *d),
			  osprd_info_t *d)
{
	int fd;
	task_lock(task);
	spin_lock(&task->files->file_lock);
	{
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 13)
		struct files_struct *f = task->files;
#else
		struct fdtable *f = task->files->fdt;
#endif
		for (fd = 0; fd < f->max_fds; fd++)
			if (f->fd[fd])
				(*hook)(f->fd[fd], d);
	}
	spin_unlock(&task->files->file_lock);
	task_unlock(task);
}


// Destroy a osprd_info_t.

static void cleanup_device(osprd_info_t *d)
{
	wake_up_all(&d->blockq);
	if (d->gd) {
		del_gendisk(d->gd);
		put_disk(d->gd);
	}
	if (d->queue)
		blk_cleanup_queue(d->queue);
	if (d->data)
		vfree(d->data);
}


// Initialize a osprd_info_t.

static int setup_device(osprd_info_t *d, int which)
{
	memset(d, 0, sizeof(osprd_info_t));

	/* Get memory to store the actual block data. */
	if (!(d->data = vmalloc(nsectors * SECTOR_SIZE)))
		return -1;
	memset(d->data, 0, nsectors * SECTOR_SIZE);
	
	/* Set up the I/O queue. */
	spin_lock_init(&d->qlock);
	if (!(d->queue = blk_init_queue(osprd_process_request_queue, &d->qlock)))
		return -1;
	blk_queue_hardsect_size(d->queue, SECTOR_SIZE);
	d->queue->queuedata = d;

	/* The gendisk structure. */
	if (!(d->gd = alloc_disk(1)))
		return -1;
	d->gd->major = OSPRD_MAJOR;
	d->gd->first_minor = which;
	d->gd->fops = &osprd_ops;
	d->gd->queue = d->queue;
	d->gd->private_data = d;
	snprintf(d->gd->disk_name, 32, "osprd%c", which + 'a');
	set_capacity(d->gd, nsectors);
	add_disk(d->gd);

	/* Call the setup function. */
	osprd_setup(d);

	return 0;
}

static void osprd_exit(void);


// The kernel calls this function when the module is loaded.
// It initializes the 4 osprd block devices.

static int __init osprd_init(void)
{
	int i, r;

	// shut up the compiler
	(void) for_each_open_file;
#ifndef osp_spin_lock
	(void) osp_spin_lock;
	(void) osp_spin_unlock;
#endif
	
	/* Register the block device name. */
	if (register_blkdev(OSPRD_MAJOR, "osprd") < 0) {
		printk(KERN_WARNING "osprd: unable to get major number\n");
		return -EBUSY;
	}

	/* Initialize the device structures. */
	for (i = r = 0; i < NOSPRD; i++)
		if (setup_device(&osprds[i], i) < 0)
			r = -EINVAL;

	if (r < 0) {
		printk(KERN_EMERG "osprd: can't set up device structures\n");
		osprd_exit();
		return -EBUSY;
	} else
		return 0;
}


// The kernel calls this function to unload the osprd module.
// It destroys the osprd devices.

static void osprd_exit(void)
{
	int i;
	for (i = 0; i < NOSPRD; i++)
		cleanup_device(&osprds[i]);
	unregister_blkdev(OSPRD_MAJOR, "osprd");
}


// Tell Linux to call those functions at init and exit time.
module_init(osprd_init);
module_exit(osprd_exit);
