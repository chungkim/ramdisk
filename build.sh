sudo rmmod osprd.ko
echo "[BUILD] kernel module osprd unloaded!"

echo "[BUILD] compiling kernel module osprd..."
sudo make clean
sudo make KBUILD_NOPEDANTIC=1
echo "[BUILD] kernel module osprd compiled!"

sudo insmod osprd.ko
echo "[BUILD] kernel module osprd loaded!"

sudo ./create-devs
echo "[BUILD] device files osprd{a-d} created!"

sudo chown chunghwn:utahstud *
echo "[BUILD] file owner changed to chunghwn"

sudo lsmod | grep "Module"
sudo lsmod | grep "osprd"
